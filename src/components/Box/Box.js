import React, { memo } from 'react';
import './Box.css';

const Box = (props) => {
  const {
    color,
    pos: { x, y } = {},
    width = 100,
    height = 100
  } = props;

  return (
    <div
      className="Box"
      style={{
        width: `${width}px`,
        height: `${height}px`,
        background: color,
        transform: `translate3d(${x}px, ${y}px, 0.01px)`,
      }}
    />
  )
};

export default memo(Box);

