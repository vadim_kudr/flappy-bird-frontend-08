import { useLayoutEffect, useRef, useCallback } from 'react';

export default function useAnimationEffect(animate) {
  const requestRef = useRef();
  const previousTimeRef = useRef();

  const animateCallback = useCallback((time) => {
    if (!previousTimeRef.current) {
      previousTimeRef.current = time;
    }

    const deltaTime = time - previousTimeRef.current;
    animate(deltaTime);

    previousTimeRef.current = time;
    requestRef.current = requestAnimationFrame(animateCallback);
  }, [animate]);

  useLayoutEffect(() => {
    requestRef.current = requestAnimationFrame(animateCallback);
    return () => cancelAnimationFrame(requestRef.current);
  }, [animateCallback]);

  return requestRef;
}