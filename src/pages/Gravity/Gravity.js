import React, { useState, useCallback, useEffect } from 'react';
import useAnimationEffect from 'hooks/useAnimationEffect';

import Box from 'components/Box/Box';

const gravity = 0.2;
const acceleration = 0.0005;

const initialState = {
  isAnimated: false,
  velocity: 0,
  pos1: { x: 100, y: 10 },
  pos2: { x: 300, y: 10 },
};

export default function Gravity() {
  const [state, setState] = useState(initialState);  

  const animate = useCallback((deltaTime) => {
    setState((state) => {
      if (!state.isAnimated) {
        return state;
      }

      const { velocity, pos1, pos2 } = state;
      const newVelocity = velocity + acceleration * deltaTime;

      return {
        ...state,
        velocity: newVelocity,
        pos1: { ...pos1, y: pos1.y + gravity * deltaTime },
        pos2: { ...pos2, y: pos2.y + newVelocity * deltaTime },
      }
    });
  }, []);

  useAnimationEffect(animate);

  useEffect(() => {
    const handleClick = () => {
      if (!state.isAnimated) {
        setState((state) => ({ ...state, isAnimated: true }));
        return;
      }
      
      setState(() => initialState);
    };

    document.documentElement.addEventListener('click', handleClick);
    return () => document.documentElement.removeEventListener('click', handleClick);
  }, [state.isAnimated]);

  return (
    <>
      <Box pos={state.pos1} color="lightblue" />
      <Box pos={state.pos2} color="tomato" />
    </>
  );
}