import React from 'react';
import {
  HashRouter,
  Switch,
  Route,
} from 'react-router-dom';

import App from 'pages/App/App';
import Gravity from 'pages/Gravity/Gravity';
import BackgroundAnimation from 'pages/BackgroundAnimation/BackgroundAnimation';

export default function Router() {
  return (
    <HashRouter>
      <>
        <Switch>
          <Route exact path="/">
            <App />
          </Route>
          <Route path="/gravity">
            <Gravity />
          </Route>
          <Route path="/background">
            <BackgroundAnimation />
          </Route>
        </Switch>
      </>
    </HashRouter>
  );
}